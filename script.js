console.warn(`Hello world`);

//exponent operator

let n1 = 2 ** 3;
console.warn(n1);

let n2 = Math.pow(2, 3);
console.warn(n2);

// template literals

let n3 = `Marvin`;

// w/o template literals

console.log('Hello ' + n3 + '!' + ' Welcome to programming!');

console.log(`Hello ${n3}! Welcome to programming!`);

//multi line

const mess = `
${n3} attended a Math competition.
He won it by solving the problem 8 ** 3 with the solution of ${n1}

`;
console.warn(mess);

const intRate = .1;
const prin = 1000;

console.log(`Interest on your account: ${prin * intRate}`);

// array destructions -- allows to unpack elems in arrays into distinct variables.
// Allows us to name arrau elements with variables instead of using the index.

let arr1 = [123,12312,32321];

let str1 = arr1.forEach(function(task){
	console.log(task);
});

console.log(`Hello ${arr1[0]} ${arr1[1]} ${arr1[2]}`);

//array destructing

const [first, second, third] = arr1;

console.warn(first);
console.warn(second);
console.warn(third);

// object destructing -- allows us to unpack properties of objects into distinct variables
// shortens the syntax for accessing properties from objects

let obj1 = {
	firstN: `Marvin`,
	mid: `Mosquera`,
	last: `Orias`
};

console.warn(obj1.firstN);
console.warn(obj1.mid);
console.warn(obj1.last);

console.log(`Hello ${obj1.firstN} ${obj1.mid} ${obj1.last}`);

//object destructure
let {firstN, mid, last} = obj1;

console.log(firstN);
console.log(mid);
console.log(last);

function name({firstN, mid, last}){
	console.log(`${firstN} ${mid} ${last}`);
}

name(obj1);

// arrow functions -- useful for code snippets where creating functions
// will not be reused in any other portion of the code
//not working on constructor functions

let arrow1 = () => {
	console.warn(`Hello world`);
};

function arrow2(){
	console.log(`Hello world`);
}

arrow1();
arrow2();

// pre arrow function and template literals
function name1(first, mid, last){
	console.warn(first + ` ` + mid + ' ' + last);
}

name1(`Marvin`, `John`, `Doe`);

let name2 = (first, second, third) => {
	console.log(`${first} ${second} ${third}`);
};

name2(`John`, `Doe`, `Boss`);

let stud1 = [`John`, `Doe`, `Big Boss`];
stud1.forEach(function(names){
	console.log(names);
});

stud1.forEach((names) => {
	console.log(names);
});

function sum(x, y){
	return x * y;
}

console.warn(sum(3, 3));

let sum1 = (x, y) => {
	 return x * y;
};

console.warn(sum1(2,5));

// defualt function argument value -- provides a default argu value if non is provided
let greet = (name = `Marvin`) => {
	return `Good day ${name}`;
}

console.warn(greet(`Big Boss`));

function greet1(name = `Marvin`){
	return `GOOD day ${name}`;
}

console.warn(greet1());

// class based object blueprints -- allows creation of objects 
// using class as blueprints

class Person {
	constructor(Name, Age){
		this.name = Name;
		this.age = Age;

		console.warn(`${this.name} ${this.age}`);
	}
}

let p1 = new Person(`Marvs`, 25);

p1.Name = `John`;
p1.Age = 30;
console.warn(p1);